<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Exception\Wrapper\Api;

use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperInterface;
use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;

/**
 * Class ExceptionWrapperClass.
 *
 * Use this class to help yourself to implement ExceptionWrapperInterface.
 *
 * @see ExceptionWrapperInterface
 *
 * @api
 *
 * @package Interactiv4\SPL\Exception\Wrapper
 */
class ExceptionWrapper implements ExceptionWrapperInterface
{
    use ExceptionWrapperTrait;
}
